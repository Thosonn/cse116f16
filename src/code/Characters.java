package code;
/**
 * @author James Deciutiis
 */
import java.awt.Color;

public enum Characters {
	MISS_SCARLET("Miss Scarlet", Color.RED),
	PROFESSOR_PLUM("Professor Plum", Color.CYAN),
	MR_GREEN("Mr Green", Color.GREEN),
	MRS_WHITE("Mrs. White", Color.WHITE),
	MRS_PEACOCK("Mrs Peacock", Color.BLUE),
	COLONEL_MUSTARD("Colonel Mustard", Color.YELLOW);
	
	/**
	 * give ability to access these two varibales - Kun Lin
	 */
	private final String name;
	private final Color c;
	Characters(String string, Color color){
		name = string;
		c = color;
	}
	public Color getColor(){
		return c;
	}
	public String getName(){
		return name;
	}
}
