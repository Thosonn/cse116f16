package code;
/**
 * @author Darren Matthews
 * @author Kun Lin
 */
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

public class Clue extends Observable {
	private ArrayList<weaponCard> _weaponCards = new ArrayList<weaponCard>();
	private ArrayList<roomCard> _roomCards = new ArrayList<roomCard>();
	private ArrayList<suspectCard> _suspectCards = new ArrayList<suspectCard>();
	private ArrayList<weaponCard> _weaponOpt = new ArrayList<weaponCard>();
	private ArrayList<suspectCard> _suspectOpt = new ArrayList<suspectCard>();
	private ArrayList<roomCard> _roomOpt = new ArrayList<roomCard>();
	private ArrayList<Cards> _culpritCards = new ArrayList<Cards>();
	private Board _b;
	private Random _r = new Random();
	private int diceNumber;
	protected static ArrayList<Player> _players = new ArrayList<Player>();
	protected static ArrayList<Player> _outOfGame = new ArrayList<Player>();
	
	
	public Clue(int numOfPlayers)  {
		diceNumber =0;
		makeBoard();
		makeCards();
		_weaponOpt.addAll(_weaponCards);
		_suspectOpt.addAll(_suspectCards);
		_roomOpt.addAll(_roomCards);
		generateCulprit();
		addPlayers(numOfPlayers);
		dealCards();
		_players.get(0).setTurn(true);
	}
	public int getDice(){
		return diceNumber;
	}
	public void setDice(int x){
		diceNumber = x;
	}
	public void makeBoard(){
		_b = new Board();
		_b.makeRooms();
		_b.setSpecialTiles();
	}
	/**
	 * @author Kun Lin
	 * This method only all player to move 1 tile at once, no diagonal move will be accepted
	 * and then the dice number is 0, it will also be invalid to move
	 * when player enter a room from door, GUI will be notified and enable the suggestion button
	 * 
	 * @param p Player try to make the move
	 * @param rownum row int try to move into
	 * @param colnum col int player try to move into
	 * @return whether its a valid move
	 */
	public boolean moveOneTileOnly (Player p, int rownum, int colnum){
		if(rownum>=Board.ROW||colnum>=Board.COL){
			System.out.println("Don't go off the board");
			return false;
		}
		if (diceNumber == 0){
		System.out.println("Out of step");
			return false;
		}
		if (rownum>=25||colnum>=24){
			return false;
		}
		int distanceInX =  Math.abs(p.getCol()-colnum);
		int distanceInY =  Math.abs(p.getRow()-rownum);
		if (distanceInX+distanceInY!=1){
			return false;	
		}
		// making sure the tile is valid and making sure that it enter the room only by the door
		boolean retVal = !_b.getTiles()[rownum][colnum].isInvalid()&&(_b.getTiles()[rownum][colnum].getIsDoor()||_b.getTiles()[rownum][colnum].getRoom().equals("None"));
		if(retVal==true){
			diceNumber -=1;
			_b.getTiles()[p.getRow()][p.getCol()].playerLeaving();
			p.setPos(rownum, colnum);
			_b.getTiles()[p.getRow()][p.getCol()].playerEntering();
			if(_b.getTiles()[p.getRow()][p.getCol()].getIsDoor()){
				//TODO entering a room handling
				setChanged();
				notifyObservers();
				
			}
		}
		else {
			System.out.println("Illegal move");
		}
		
		return retVal;
	}
	/**
	 *  @author Darren Matthews
	 *  @author James Deciutiis
	 *  @author Kun Lin
	 *  I made this a separate method because I think it easier to display it, by having a taking secret passage
	 *  button in the GUI, and then clicking on it, instead of clicking on the passage position on the board
	 *  also I think it better to separate this from move method since this is a special kind of move - Kun
	 *  
	 *  this method first check if the player is in a room, if not return false, then it would
	 *  check which room the player is in and set the player's location accordingly, I was think if two
	 *  player take the same secret passage we should display the player's position in GUI a little different
	 *  without overlapping - Kun
	 * @return boolean value true possible move, false otherwise
	 */
	public boolean takingSecretPassage(){
		Player p = playerTurn();
		if(_b.getTiles()[p.getRow()][p.getCol()].getRoom()=="None"){
			System.out.println("Not in a room");
			return false;
		}
		if(_b.getTiles()[p.getRow()][p.getCol()].getRoom() == "Study"){
			p.setPos(18, 19);
			setChanged();
			notifyObservers();
			return true;
		}
		if(_b.getTiles()[p.getRow()][p.getCol()].getRoom() == "Kitchen"){
			p.setPos(3, 5);
			setChanged();
			notifyObservers();
			return true;
		}
		if(_b.getTiles()[p.getRow()][p.getCol()].getRoom() == "Lounge"){
			p.setPos(19, 4);
			setChanged();
			notifyObservers();
			return true;
		}
		if(_b.getTiles()[p.getRow()][p.getCol()].getRoom() == "Conservatory"){
			p.setPos(5, 18);
			setChanged();
			notifyObservers();
			return true;
		}
		System.out.println("this room does not have secret passage");
		return false;
	}

	private void makeCards(){
		_weaponCards.add(new weaponCard("Wrench"));
		_weaponCards.add(new weaponCard("Candlestick"));
		_weaponCards.add(new weaponCard("Lead Pipe"));
		_weaponCards.add(new weaponCard("Rope"));
		_weaponCards.add(new weaponCard("Revolver"));
		_weaponCards.add(new weaponCard("Knife"));
		_roomCards.add(new roomCard("Study"));
		_roomCards.add(new roomCard("Kitchen"));
		_roomCards.add(new roomCard("Hall"));
		_roomCards.add(new roomCard("Conservatory"));
		_roomCards.add(new roomCard("Lounge"));
		_roomCards.add(new roomCard("Ballroom"));
		_roomCards.add(new roomCard("Dining Room"));
		_roomCards.add(new roomCard("Library"));
		_roomCards.add(new roomCard("Billiard Room"));
		_suspectCards.add(new suspectCard("Colonel Mustard"));
		_suspectCards.add(new suspectCard("Miss Scarlet"));
		_suspectCards.add(new suspectCard("Professor Plum"));
		_suspectCards.add(new suspectCard("Mr.Green"));
		_suspectCards.add(new suspectCard("Mrs.White"));
		_suspectCards.add(new suspectCard("Mrs.Peacock"));
	}

	private void generateCulprit(){
		int r = _r.nextInt(6);
		_culpritCards.add(_weaponCards.get(r));  //pulls a random card from weapons arraylist and removes it
		_weaponCards.remove(r);
		
		r = _r.nextInt(9);
		_culpritCards.add(_roomCards.get(r));  //pulls a random card from rooms arraylist and removes it
		_roomCards.remove(r);
		
		r = _r.nextInt(6);
		_culpritCards.add(_suspectCards.get(r));  //pulls a random card from suspect arraylist and removes it
		_suspectCards.remove(r);
	}
	/**
	 * checking whos turn it is
	 * @return the player that is playing
	 */
	public Player playerTurn(){
		for (int i=0;i<_players.size();i++){
			if (_players.get(i).getTurn()==true){
				return _players.get(i);
			}
		}
		return null;
	}
	/**
	 * finding the player that is player and set its
	 * status to be false and
	 * set next player's turn to be true 
	 */
	public void nextPlayerTurn(){
		int i =_players.indexOf(playerTurn());
		if (i<_players.size()-1){
			_players.get(i).setTurn(false);
			_players.get(i+1).setTurn(true);
		}
		else if (i==_players.size()-1){
			_players.get(i).setTurn(false);
			_players.get(0).setTurn(true);
		}
	}
	private void addPlayers(int numPlayers){
		//creates players and sets their starting positions
		for(int i = 0; i < numPlayers; i++){
			Player temp = new Player(Characters.values()[i], //grabs character enum 
									(_b.getStartingTiles().get(i).getRow()),//get the row of a starting tile
									(_b.getStartingTiles().get(i).getCol()),i);//get the col of a starting tile			            
			_players.add(temp);
		}
	}
	/**
	 * @author Kun Lin
	 * the first loop would check from the left ot the person which is 1 int more in the 
	 * array list has the card, once it gets to the size of the array list
	 * it start at zero to continued check
	 * if the player making suggestion is at 0, the second loop will not do anything
	 * The method checks if player that answering suggestion card the method that
	 * makeSuggestion() would return a empty list of Cards 
	 * if its not empty the player and the first card in the list would be set as 
	 * parameter for PlayerThatAnswerSuggestion Objct
	 * @param p player making suggestion
	 * @param room suggested room
	 * @param suspect suggested suspect
	 * @param weapon suggested weapon
	 * @return PlayerThatAnswerSuggestion objest that has player and the card answered suggestion
	 */
	public PlayerThatAnswerSuggestion  MakingSuggestion(Player p, roomCard room, suspectCard suspect, weaponCard weapon ){
		PlayerThatAnswerSuggestion retVal = new PlayerThatAnswerSuggestion(null,null);
		Random r = new Random();
		int x = _players.indexOf(p);
		for (int i=x+1;i<_players.size();i++){
			if(i<_players.size()){
				ArrayList<Cards> matchCards =p.makeSuggestion(_players.get(i), room, suspect, weapon);
			if (!matchCards.isEmpty()){
				retVal = new PlayerThatAnswerSuggestion(_players.get(i),matchCards.get(r.nextInt(matchCards.size())));
				return retVal;
			}
			}
		}
		for (int i=0;i<x;i++){
			if(x!=0){
				ArrayList<Cards> matchCards =p.makeSuggestion(_players.get(i), room, suspect, weapon);
			if (!matchCards.isEmpty()){
				retVal = new PlayerThatAnswerSuggestion(_players.get(i),matchCards.get(r.nextInt(matchCards.size())));
				return retVal;
			}
			}
		}
		return retVal;
	}
	public boolean makeAccusation(roomCard room, suspectCard suspect, weaponCard weapon ){
		Integer check = 0;
		for(int i=0;i<_culpritCards.size();i++){
			if (room.getName().equals(_culpritCards.get(i).getName())){
				check +=1;
			}
			if (suspect.getName().equals(_culpritCards.get(i).getName())){
				check +=1;
			}
			if (weapon.getName().equals(_culpritCards.get(i).getName())){
				check +=1;
			}
		}
		return check ==3;
	}
	private void dealCards(){
		//combines all the cards into one arraylist
		ArrayList<Cards> combined = new ArrayList<Cards>();
		combined.addAll(_weaponCards);
		combined.addAll(_roomCards);
		combined.addAll(_suspectCards);
		int r;
		//picks a card randomly and deals it out to a player
		while(!combined.isEmpty()){
			int i = 0;
			for(i = 0; i < _players.size(); i++){
				if(combined.size() != 0){
					r = _r.nextInt(combined.size());
					_players.get(i).addCard(combined.get(r));
					combined.remove(r);
				}
			}
		}
	}
	public void selectingExistDoor(){
		
	}
	
	public ArrayList<Cards> getCulpritCards(){
		return _culpritCards;
	}
	public ArrayList<Player> getPlayers(){
		return _players;
	}
	public ArrayList<Player> getLosePlayers(){
		return _outOfGame;
	}
	public Board getBoard(){
		return _b;
	}
	public  ArrayList<weaponCard> getWeaponOpt(){
		return _weaponOpt;
	}
	public  ArrayList<roomCard> getRoomOpt(){
		return _roomOpt;
	}
	public  ArrayList<suspectCard> getSuspectOpt(){
		return _suspectOpt;
	}
}
