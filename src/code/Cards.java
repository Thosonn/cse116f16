package code;
/**
 * @author Darren Matthews
 */
public class Cards {
	
	private String _name;
	private String _type;
	
	public Cards(String type, String name){
		_type = type;
		_name = name;
	}
	public String getName(){
		return _name;
	}
	public String getType(){
		return _type;
	}
}
