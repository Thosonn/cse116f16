package code;

import java.util.ArrayList;
import java.util.Random;
/**
 * @author Darren Matthews
 * @author James Deciutiis
 * @author Kun Lin
 */
public class Player {
	private Characters _character;
	private int _rowPos;
	private int _colPos;
	private int _playerNum;
	private boolean turn;
	Tiles [][]tiles = Board._t;
	private ArrayList<Cards> _cards = new ArrayList<Cards>();
	
	public Player(Characters charact, int row, int col, int plyrNum){
		turn = false;
		_character = charact;
		_rowPos = row;
		_colPos = col;
		_playerNum = plyrNum;
	}
	
	public int rollDie(){
		Random r = new Random();
		return r.nextInt(5) + 1;
	}
	
	public int getRow(){return _rowPos;}
	public int getCol(){return _colPos;}
	
		/**
	 * @author Kun Lin
	 * the method call it self only allow player to move 1 square in 4 direction
	 * up down right left
	 * the player had to choose the player they want to move on, and then the tile player used
	 * to stand will change back valid tile so other player could enter
	 * the tile player move on to would perhaps set to invalid if its hallway tile
	 * 	(x,y) is a point indicate where the player try to move to
	 * @param colnum col int
	 * @param rownum row int
	 * @return	ture if these move is valid, false other wise
	 */
	public boolean moveOneTileOnly ( int colnum, int rownum){
		int distanceInX =  Math.abs(_colPos-colnum);
		int distanceInY =  Math.abs(_rowPos-rownum);
		if (distanceInX+distanceInY!=1){
			return false;	
		}
		// making sure the tile is valid and making sure that it enter the room only by the door
		boolean retVal = !tiles[rownum][colnum].isInvalid()&&(tiles[rownum][colnum].getIsDoor()||tiles[rownum][colnum].getRoom().equals("None"));
		if(retVal==true){
			tiles[_rowPos][_colPos].playerLeaving();
			_rowPos = rownum;
			_colPos = colnum;
			tiles[_rowPos][_colPos].playerEntering();
			if(tiles[rownum][colnum].getIsDoor()){
				//TODO entering a room handling
			}
		}
		return retVal;
	}
	// Darren's move method
	public boolean move(int roll,int row,int col){
		int preRow = _rowPos;
		int preCol = _colPos;
		boolean skipRow = false;
		//out of bounds, cant move
		if(row > 24 || col > 23 || row < 0 || col < 0){
			System.out.println("out of bounds");
			return false;
		}
		//if moving to a secret passage
		if(tiles[row][col].getRoom() == tiles[_rowPos][_colPos].getRoom() && tiles[row][col].getIsSecretDoor()){
			if(tiles[row][col].getRoom() == "Study"){
				_rowPos = 23;
				_colPos = 18;
			}
			if(tiles[row][col].getRoom() == "Kitchen"){
				_rowPos = 3;
				_colPos = 0;
			}
			if(tiles[row][col].getRoom() == "Lounge"){
				_rowPos = 23;
				_colPos = 18;
			}
			if(tiles[row][col].getRoom() == "Conservatory"){
				_rowPos = 19;
				_colPos = 1;
			}
			return true;
		}
		//if space is invalid or trying to enter a room without using the door
		if(tiles[row][col].isInvalid() || (!tiles[row][col].getRoom().equals("None") && !tiles[row][col].getIsDoor())){
			System.out.println("end tile is invalid or room and not door");
			return false;
		}
		
		//if leaving a room
		if(tiles[row][col].getRoom().equals("None") && !tiles[_rowPos][_colPos].getRoom().equals("None")){
			//not done
		}
		
		//for loop moves piece one space at a time in the direction of the coords
		//if statements check if the move is legal (into a room without using a door,invalid,out of bounds etc.)
		for(int i = 0; i < roll; i++){	
			if ((_rowPos < row && !tiles[_rowPos + 1][_colPos].isInvalid()) &&
				(tiles[_rowPos + 1][_colPos].getRoom().equals("None") || tiles[_rowPos + 1][_colPos].getIsDoor()) && !skipRow){
				_rowPos += 1;
			} 
			else if ((_rowPos > row && !tiles[_rowPos - 1][_colPos].isInvalid()) &&
				(tiles[_rowPos - 1][_colPos].getRoom().equals("None") ||tiles[_rowPos - 1][_colPos].getIsDoor()) && !skipRow){
				_rowPos -= 1;
			}
			else if ((_colPos > col && !tiles[_rowPos][_colPos - 1].isInvalid()) &&
					(tiles[_rowPos][_colPos - 1].getRoom().equals("None") || tiles[_rowPos][_colPos - 1].getIsDoor())){
				_colPos -= 1;
				skipRow = false;
			}
			else if ((_colPos < col && !tiles[_rowPos][_colPos + 1].isInvalid()) &&
					(tiles[_rowPos][_colPos + 1].getRoom().equals("None") || tiles[_rowPos][_colPos + 1].getIsDoor())){
				_colPos += 1;
				skipRow = false;
			}
			//rest of these if statements are for special cases like when
			//the piece is blocked from moving in a straight line
			else if (row == _rowPos && _colPos > col && (tiles[_rowPos][_colPos - 1].isInvalid() ||
					!tiles[_rowPos][_colPos - 1].getRoom().equals("None") || !tiles[_rowPos][_colPos - 1].getIsDoor())){
				if(_rowPos == 10 || _rowPos == 6)
					_rowPos += 1;
				else
					_rowPos -= 1;
				skipRow = true;
			}
			else if (row == _rowPos && _colPos < col && (tiles[_rowPos][_colPos + 1].isInvalid() ||
					!tiles[_rowPos][_colPos + 1].getRoom().equals("None") || !tiles[_rowPos][_colPos + 1].getIsDoor())){
				if(_rowPos == 10 || _rowPos == 6)
					_rowPos += 1;
				else
					_rowPos -= 1;
				skipRow = true;
			}
			else if (col == _colPos && _rowPos > row && (tiles[_rowPos - 1][_colPos].isInvalid() ||
					!tiles[_rowPos - 1][_colPos].getRoom().equals("None") || !tiles[_rowPos - 1][_colPos].getIsDoor())){
				if(_colPos < 7 || (_colPos > 12 && _colPos < 16))
					_colPos += 1;
				else
					_colPos -= 1;
			}
			else if (col == _colPos && _rowPos < row && (tiles[_rowPos + 1][_colPos].isInvalid() ||
					!tiles[_rowPos + 1][_colPos].getRoom().equals("None") || !tiles[_rowPos + 1][_colPos].getIsDoor())){
				if(_colPos < 7 || (_colPos > 12 && _colPos < 16))
					_colPos += 1;
				else
					_colPos -= 1;
			}
//			System.out.println(_rowPos + "," + _colPos);
		}
		if (_rowPos == row && _colPos == col){
			tiles[preRow][preCol].setHasPlayer(-1);//set old tile position to having no player
			tiles[_rowPos][_colPos].setHasPlayer(_playerNum);//set new position to current player 
			return true;//if the position of the piece equals the position wanted then return true
		}
		else{
//			System.out.println("Ending row: " + _rowPos);
//			System.out.println("Endinc col: " + _colPos);
			_rowPos = preRow;
			_colPos = preCol;
			return false;//otherwise reset the loation and return false
		}
	}
	public void addCard(Cards c){_cards.add(c);}
	
	public void setPos(int row,int col){
		_rowPos = row;
		_colPos = col;
	}
	public boolean getTurn(){
		return turn;
	}
	public void setTurn (boolean b){
		turn = b;
	}
	public boolean hasCard(Cards c){return _cards.contains(c);}
	
	/**
	 * @author James Deciutiis 
	 * @param player; the player that is being asked the suggestion
	 * @param room; the room being suggested
	 * @param suspect; the suspect being suggested
	 * @param weapon; the weapon being suggested
	 * @return an arraylist containing all the suggested cards that each player had :)
	 */
	public ArrayList<Cards> makeSuggestion(Player player, roomCard room, suspectCard suspect, weaponCard weapon ){
        ArrayList<Cards> cards = player.getCards();
        ArrayList<Cards> suggestedCards = new ArrayList<Cards>();
        for(int i =0; i<cards.size();i++){
        if(cards.get(i).getName().equals(room.getName())){
        	suggestedCards.add(room);
        	}
        if(cards.get(i).getName().equals(suspect.getName())){
                 suggestedCards.add(suspect);
            }
        if(cards.get(i).getName().equals(weapon.getClass())){
            suggestedCards.add(weapon);
        	}
        }
        return suggestedCards;
    }
	
	public ArrayList<Cards> getCards(){
        return _cards;
    }

//	public boolean makeAccusation(String room, String suspect, String weapon){
//		//same as suggestion but if a player has the card,whoever makes the suggestion should be removed from the players list
//	}
	public Characters getCharacter(){return _character;}
	public String getRoom(){return tiles[_rowPos][_colPos].getRoom();}
	public int getPlayerNum(){return _playerNum;}
	public void emptyHand(){_cards.removeAll(_cards);}
	
}
