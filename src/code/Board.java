package code;

import java.util.ArrayList;
/**
 * @author Darren Matthews
 * @author James Deciutiis
 */
public class Board {
 
	public static final int ROW = 25;
	public static final int COL = 24;
	protected static Tiles _t[][] = new Tiles[ROW][COL];
	private ArrayList<Tiles> _starts = new ArrayList<Tiles>();
	
	
	public Board(){
		// (0,0) is the top left of the board
		for(int i = 0; i<ROW; i++){
			for(int j = 0; j<COL; j++){
				_t[i][j] = new Tiles(i,j);
			}	
		}
	}
	
	public void makeRooms(){
		//Creates the Study room by naming tiles
		for(int i = 0; i<4; i++){
			for(int j = 0; j<7; j++){
				_t[i][j].setRoom("Study");
				}
			}
		//Creates the Library room by naming tiles
		for(int i = 6; i<11; i++){
			for(int j = 0; j<7; j++){
				_t[i][j].setRoom("Library");
				}
			}
		//Creates the Billiard Room room by naming tiles
		for(int i = 12; i<17; i++){
			for(int j = 0; j<6; j++){
				_t[i][j].setRoom("Billiard Room");
				}
			}
		//Creates the Conservatory room by naming tiles
		for(int i = 19; i<24; i++){
			for(int j = 0; j<6; j++){
				_t[i][j].setRoom("Conservatory");
			}
		}
		//Creates the Hall room by naming tiles
		for(int i = 0; i<7; i++){
			for(int j = 9; j<15; j++){
				_t[i][j].setRoom("Hall");
			}
		}
		//Creates the Lounge room by naming tiles
		for(int i = 0; i<6; i++){
			for(int j = 17; j<24; j++){
				_t[i][j].setRoom("Lounge");
			}
		}
		//Creates the Dining Room room by naming tiles
		for(int i = 9; i<16; i++){
			for(int j = 16; j<24; j++){
				_t[i][j].setRoom("Dining Room");
			}
		}
		//Creates the Kitchen room by naming tiles
		for(int i = 18; i<24; i++){
			for(int j = 18; j<24; j++){
				_t[i][j].setRoom("Kitchen");
			}
		}
		//Creates the Ballroom room by naming tiles
		for(int i = 17; i<24; i++){
			for(int j = 8; j < 16; j++){
				_t[i][j].setRoom("Ballroom");
			}
		}
		//middle of the board
		for(int i = 8; i<15; i++){
			for(int j = 9; j < 14; j++){
				_t[i][j].setInvalid();
			}
		}
		//bottom row
		for(int j = 0; j < 9; j++){
			_t[24][j].setInvalid();
		}
		for(int j = 15; j < 24; j++){
			_t[24][j].setInvalid();
		}
	}
	
	public void setSpecialTiles(){
		//making changes to the "special" tiles in the block that was just created
		
		//by study
		_t[0][6].setInvalid();
		_t[3][5].setIsDoor();
		_t[3][0].setIsSecretDoor();
		_t[4][0].setInvalid();
		
		//by library
		_starts.add(_t[5][0]);
		
		_t[6][0].setInvalid();
		_t[6][6].setRoom("None");
		_t[8][6].setIsDoor();
		_t[10][0].setInvalid();
		_t[10][6].setRoom("None");
		_t[10][3].setIsDoor();
		_t[11][0].setInvalid();
		
		//by billiard room
		_t[12][1].setIsDoor();
		_t[15][5].setIsDoor();
		_t[17][0].setInvalid();
		
		//by conservatory
		//_t[18][0].setIsStart();
		_starts.add(_t[18][0]);
		
		_t[19][1].setIsSecretDoor();
		_t[19][0].setInvalid();
		_t[19][4].setIsDoor();
		
		//by ballroom
		_t[19][8].setIsDoor();
		_t[17][9].setIsDoor();
		_t[19][15].setIsDoor();
		_t[17][14].setIsDoor();
		
	//	_t[24][9].setIsStart();
		_starts.add(_t[24][9]);
		
		_t[24][10].setInvalid();
		_t[24][11].setInvalid();
		_t[24][12].setInvalid();
		_t[24][13].setInvalid();
		
		//_t[24][14].setIsStart();
		_starts.add(_t[24][14]);
		
		_t[23][6].setInvalid();
		_t[23][8].setRoom("None");
		_t[23][9].setRoom("None");
		_t[23][14].setRoom("None");
		_t[23][15].setRoom("None");
		
		//by kitchen
		_t[23][17].setInvalid();
		_t[23][18].setIsSecretDoor();
		_t[18][19].setIsDoor();
		_t[18][23].setInvalid();
		_t[16][23].setInvalid();
		
		//by dining room
		_t[15][18].setRoom("None");
		_t[15][17].setRoom("None");
		_t[15][16].setRoom("None");
		_t[12][16].setIsDoor();
		_t[9][17].setIsDoor();
		_t[8][23].setInvalid();
		//_t[7][23].setIsStart();
		_starts.add(_t[7][23]);
		
		//by lounge
		_t[6][23].setInvalid();
		_t[5][23].setIsSecretDoor();
		_t[5][18].setIsDoor();
		_t[0][17].setInvalid();
		_starts.add(_t[0][16]);
		
		_t[0][15].setInvalid();
		_t[4][9].setIsDoor();
		_t[6][11].setIsDoor();
		_t[6][12].setIsDoor();
		_t[0][8].setInvalid();
	}
	
	public Tiles[][] getTiles(){
		return _t;
	}
	public ArrayList<Tiles> getStartingTiles(){
		return _starts;
	}
}
