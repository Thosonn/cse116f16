package code;
/**
 * @author Darren Matthews
 * 
 */
public class Tiles {
	private int _row;
	private int _col;
	private String _room = "None";
	private boolean _isDoor = false;
	private boolean _invalid = false;
	private boolean _isSecretDoor = false;
	private boolean _isStart = false;
	private boolean _hasPlayer = false;
	private int _playerNum = -1;

	public Tiles(int row, int col){
		_row=row;
		_col=col;
	}
	/**
	 * @author Kun Lin
	 * this method will set the tile player will be walking on into invalid if
	 * its a hallway tile, if its room other player could still walk on it
	 */
	public void playerEntering(){
		if(_room =="None"){
			_invalid = true;
		}
	}
	public void playerLeaving(){
		_invalid = false;
	}
	public int getRow(){return _row;}
	public int getCol(){return _col;}
	
	public void setRoom(String room){_room = room;}
	public void setInvalid(){_invalid = true;}
	public void setIsDoor(){_isDoor = true;}
	public void setIsSecretDoor(){_isSecretDoor = true;}
	public void setIsStart(){_isStart = true;}
	public boolean setHasPlayer(int playerNum){
		_playerNum = playerNum;
		return _hasPlayer;
	}
	
	
	public boolean isInvalid(){return _invalid;}
	public boolean getIsSecretDoor(){return _isSecretDoor;}
	public boolean getIsDoor(){return _isDoor;}
	public boolean getHasPlayer(){return _hasPlayer;}
	public int getPlayerNum(){return _playerNum;}
	public String getRoom(){return _room;}
	public boolean getIsStart(){return _isStart;}
}
