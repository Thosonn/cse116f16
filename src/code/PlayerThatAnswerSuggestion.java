package code;

public class PlayerThatAnswerSuggestion {
	Cards card;
	Player player;
	public PlayerThatAnswerSuggestion(Player p, Cards c){
		card = c;
		player = p;
	}
	public Cards getAnswerCard(){
		return card;
	}
	public Player getAnswerPlayer(){
		return player;
	}
}
