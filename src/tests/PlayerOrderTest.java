package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Clue;

public class PlayerOrderTest {
	/**
	 * testing UserStory #1 : As a player, I want turns to follow the order that 
	 * Miss Scarlet goes first (this is from the rules), 
	 * then Professor Plum, Mr. Green, Mrs. White, Mrs. Peacock, and, finally, Col. Mustard so we can have a simple ordering. 
	 */
		private Clue _c;
		public PlayerOrderTest (){
			_c = new Clue(6);
		}
		@Test public void firstPlayer(){
			//first player is avaibliable to make move when the game begin
			assertTrue(_c.getPlayers().get(0).getTurn());
			assertTrue(_c.getPlayers().get(0).getCharacter().getName().equals("Miss Scarlet"));
		}
		@Test public void testingPlayerOrderInTheList(){
			//first player already tested on the method above
			String s = "Professor Plum";
			assertTrue(_c.getPlayers().get(1).getCharacter().getName().equals(s));
			s = "Mr Green";
			assertTrue(_c.getPlayers().get(2).getCharacter().getName().equals(s));
			s = "Mrs. White";
			assertTrue(_c.getPlayers().get(3).getCharacter().getName().equals(s));
			s = "Mrs Peacock";
			assertTrue(_c.getPlayers().get(4).getCharacter().getName().equals(s));
			s = "Colonel Mustard";
			assertTrue(_c.getPlayers().get(5).getCharacter().getName().equals(s));
		}
		@Test public void nextPlayerCheck(){
			_c.nextPlayerTurn();
			assertTrue(_c.getPlayers().get(0).getTurn()==false);
			assertTrue(_c.getPlayers().get(1).getTurn()==true);
			assertTrue(_c.getPlayers().get(2).getTurn()!=true);
			_c.nextPlayerTurn();
			assertTrue(_c.getPlayers().get(1).getTurn()==false);
			assertTrue(_c.getPlayers().get(2).getTurn()==true);
		}
		/**
		 * playerTurn() is a method in Clue class that will return the player that 
		 * has turn status set to be true
		 */
		@Test public void testPlayerTurnMethod(){
			Clue temp = new Clue(6);
			assertTrue(temp.playerTurn().equals(temp.getPlayers().get(0)));
			temp.nextPlayerTurn();
			assertTrue(temp.playerTurn().equals(temp.getPlayers().get(1)));

		}
}
