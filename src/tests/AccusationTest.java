package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Clue;
import code.roomCard;
import code.suspectCard;
import code.weaponCard;

public class AccusationTest {
	/**
	 * @author Kun Lin
	 * testing accusationMethod
	 */
	private Clue _c;
	public AccusationTest(){
		_c = new Clue(6);
	}
	@Test public void validAccusation(){
		while(!_c.getCulpritCards().isEmpty()){
		_c.getCulpritCards().remove(0);
		}
		_c.getCulpritCards().add(new roomCard("Ballroom"));
		_c.getCulpritCards().add(new weaponCard("Knife"));
		_c.getCulpritCards().add(new suspectCard("Mrs.White"));
		assertTrue(_c.makeAccusation(new roomCard("Ballroom"), new suspectCard("Mrs.White"), new weaponCard("Knife")));
		// invalid accusation when room is wrong
		assertFalse(_c.makeAccusation(new roomCard("Kitchen"), new suspectCard("Mrs.White"), new weaponCard("Knife")));
		// invalid accusation when suspect is wrong
		assertFalse(_c.makeAccusation(new roomCard("Ballroom"), new suspectCard("Mr.Kun"), new weaponCard("Knife")));
		// invalid accusation when wespon is wrong		
		assertFalse(_c.makeAccusation(new roomCard("Ballroom"), new suspectCard("Mrs.White"), new weaponCard("Wrench")));
		
	}
}
