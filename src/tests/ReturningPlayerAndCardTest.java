package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Clue;
import code.PlayerThatAnswerSuggestion;
import code.roomCard;
import code.suspectCard;

public class ReturningPlayerAndCardTest {
	/**
	 * @author Kun Lin
	 * Testing Last UserStory: As a player, I want to know which player and the specific card they 
	 * have that proves my suggestion is false so that I understand why it was wrong.
	 */
	private Clue c;
	public ReturningPlayerAndCardTest(){
		c = new Clue(6);
	}
	/**
	 * test when no one can answer the suggestion
	 */
	@Test public void noOneCanAnswer(){
		 roomCard testCard1 = new roomCard("Study");
		 roomCard testCard2 = new roomCard("Lounge");
	       suspectCard testCard3 = new suspectCard("Colonel Mustard");
	       c.getPlayers().get(3).addCard(testCard1);
	       c.getPlayers().get(3).addCard(testCard2);
	       PlayerThatAnswerSuggestion test = c.MakingSuggestion(c.getPlayers().get(2), null, testCard3, null);
	       assertNull(test.getAnswerCard());
	       assertNull(test.getAnswerPlayer());
	}
	/**
	 * test when next person can answer the suggestion
	 */
	@Test public void nextPlayerCanAnswer(){
		 roomCard testCard1 = new roomCard("Study");
		 roomCard testCard2 = new roomCard("Lounge");
	       suspectCard testCard3 = new suspectCard("Colonel Mustard");
	       c.getPlayers().get(3).addCard(testCard1);
	       c.getPlayers().get(3).addCard(testCard2);
	       PlayerThatAnswerSuggestion test = c.MakingSuggestion(c.getPlayers().get(2), testCard1, testCard3, null);
	       assertTrue(test.getAnswerCard().equals(testCard1));
	       assertTrue(test.getAnswerPlayer().equals(c.getPlayers().get(3)));
	}
	/**
	 * test when nextnext person can answer the suggestion
	 */
	@Test public void nextNextPlayerCanAnswer(){
		 roomCard testCard1 = new roomCard("Study");
		 roomCard testCard2 = new roomCard("Lounge");
	       suspectCard testCard3 = new suspectCard("Colonel Mustard");
	       c.getPlayers().get(3).addCard(testCard1);
	       c.getPlayers().get(4).addCard(testCard2);
	       PlayerThatAnswerSuggestion test = c.MakingSuggestion(c.getPlayers().get(2), testCard2, testCard3, null);
	       assertTrue(test.getAnswerCard().equals(testCard2));
	       assertTrue(test.getAnswerPlayer().equals(c.getPlayers().get(4)));
	}
	/**
	 * test when Last Player can answer the suggestion
	 */
	@Test public void LastPlayerCanAnswer(){
		 roomCard testCard1 = new roomCard("Study");
		 roomCard testCard2 = new roomCard("Lounge");
	       suspectCard testCard3 = new suspectCard("Colonel Mustard");
	       c.getPlayers().get(3).addCard(testCard1);
	       c.getPlayers().get(1).addCard(testCard2);
	       PlayerThatAnswerSuggestion test = c.MakingSuggestion(c.getPlayers().get(2), testCard2, testCard3, null);
	       assertTrue(test.getAnswerCard().equals(testCard2));
	       assertTrue(test.getAnswerPlayer().equals(c.getPlayers().get(1)));
	}
	/**
	 * test when  suggesting Player have card but no one can answer the suggestion
	 */
	@Test public void NoPlayerCanAnswerButSuggestedPlayerHaveMatchCard(){
		 roomCard testCard1 = new roomCard("Study");
		 roomCard testCard2 = new roomCard("Lounge");
	       suspectCard testCard3 = new suspectCard("Colonel Mustard");
	       c.getPlayers().get(3).addCard(testCard1);
	       c.getPlayers().get(2).addCard(testCard2);
	       PlayerThatAnswerSuggestion test = c.MakingSuggestion(c.getPlayers().get(2), testCard2, testCard3, null);
	       assertNull(test.getAnswerCard());
	       assertNull(test.getAnswerPlayer());
	}
}
