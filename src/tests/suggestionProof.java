package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Cards;
import code.Clue;
import code.Player;
import code.roomCard;
import code.suspectCard;
import code.weaponCard;

import java.util.ArrayList;
/**
 * @author James Deciutiis
 */
public class suggestionProof {
    @Test
    public void testOneCard() {
        Clue c = new Clue(2);
        
        roomCard testCard = new roomCard("Study");
        Player player1 = c.getPlayers().get(0);
        player1.addCard(testCard);
        Player player2 = c.getPlayers().get(1);
        player2.addCard(testCard);
    
        ArrayList<Cards> suggestedHand = player2.makeSuggestion(player1, testCard ,new suspectCard("Colonel Mustard") , new weaponCard("Wrench"));
        assertTrue(suggestedHand.contains(testCard));
    }
    @Test
    public void testTwoCards() {
        Clue c = new Clue(2);
        roomCard testCard = new roomCard("Study");
        suspectCard testCard2 = new suspectCard("Colonel Mustard");
        Player player1 = c.getPlayers().get(0);
        Player player2 = c.getPlayers().get(1);
        player1.addCard(testCard);
        player1.addCard(testCard2);
    
        ArrayList<Cards> suggestedHand = player2.makeSuggestion(player1, testCard ,testCard2 , new weaponCard("Wrench"));
        assertTrue(suggestedHand.contains(testCard));
        assertTrue(suggestedHand.contains(testCard2));
    }
    @Test
    public void testThreeCards() {
        Clue c = new Clue(2);
        roomCard testCard = new roomCard("Study");
        suspectCard testCard2 = new suspectCard("Colonel Mustard");
        weaponCard testCard3 = new weaponCard("Wrench");
        Player player1 = c.getPlayers().get(0);
        Player player2 = c.getPlayers().get(1);
        player1.addCard(testCard);
        player1.addCard(testCard2);
        player1.addCard(testCard3);
    
        ArrayList<Cards> suggestedHand = player2.makeSuggestion(player1, testCard ,testCard2 , testCard3);
        assertTrue(suggestedHand.contains(testCard));
        assertTrue(suggestedHand.contains(testCard2));
        assertTrue(suggestedHand.contains(testCard3));
    }
    @Test
    public void testNextNextPlayer() {
        Clue c = new Clue(2);
        ArrayList<Cards> suggestedHand = new ArrayList<Cards>();
        roomCard testCard = new roomCard("Study");
        suspectCard testCard2 = new suspectCard("Colonel Mustard");
        Player player1 = c.getPlayers().get(0);
        Player player2 = c.getPlayers().get(1);
        Player player3 = c.getPlayers().get(2);
        player1.addCard(testCard);
        player1.addCard(testCard2);
        player2.addCard(testCard);
        player3.addCard(testCard2);
        
        for(int i = 0; i< c.getPlayers().size(); i++){
        	 suggestedHand = c.getPlayers().get(i).makeSuggestion(player1, testCard ,testCard2 , new weaponCard("Wrench"));
        }
       
        assertTrue(suggestedHand.contains(testCard));
        assertTrue(suggestedHand.contains(testCard2));
    }
    @Test
    public void testNoCards() {
        Clue c = new Clue(2);
        Player player1 = c.getPlayers().get(0);
        Player player2 = c.getPlayers().get(1);
        
        ArrayList<Cards> suggestedHand = player2.makeSuggestion(player1, new roomCard("Study") ,new suspectCard("Colonel Mustard") , new weaponCard("Wrench"));
        assertTrue(suggestedHand.isEmpty());
    }
}