package tests;
import org.junit.Test;
import static org.junit.Assert.*;
import code.*;

public class MoveTest {
	/**
	 * this test class test the move method in clue class that we're going to 
	 * use for the GUI implementation
	 * @author Kun Lin
	 */
	private Clue c;
	public MoveTest(){
		c = new Clue(6);
		c.setDice(5);
	//	cb = new clueBoard(c);
	}
	
	@Test public void moveHorizontal(){
		c.getPlayers().get(0).setPos(5,0);
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),5,1));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),5,2));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),5,3));
	}
	@Test public void moveVeritically(){
		c.getPlayers().get(0).setPos(5,7);
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),6,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),8,7));
	}
	
	@Test public void MoveHoriAndVert(){
		c.getPlayers().get(0).setPos(5,7);
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),6,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,8));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,9));
	}
	@Test public void OutOfStep(){
		c.getPlayers().get(0).setPos(5,7);
		c.setDice(4);
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),6,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,7));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,8));
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),7,9));
		assertFalse(c.moveOneTileOnly(c.getPlayers().get(0),7,10));
	}
	@Test public void MoveIntoRoom(){
		c.getPlayers().get(0).setPos(4,5);//infront of the door
		assertTrue(c.moveOneTileOnly(c.getPlayers().get(0),3,5));//door/room
	}
	@Test public void CannotGoIntoWall(){
		c.getPlayers().get(0).setPos(5,5);
		assertFalse(c.moveOneTileOnly(c.getPlayers().get(0),6,5));
	}
	@Test public void NotAContigugousMove() {
		c.getPlayers().get(0).setPos(5,0);
		assertFalse(c.moveOneTileOnly(c.getPlayers().get(0),5,2));	
	}
	@Test public void CannotMakeDiagonalMove() {
		c.getPlayers().get(0).setPos(5,0);
		assertFalse(c.moveOneTileOnly(c.getPlayers().get(0),4,1));	
	}
	@Test public void takingSecretPassage(){
		c.getPlayers().get(0).setPos(3, 6);
		c.takingSecretPassage();
		Player p = c.getPlayers().get(0);
		assertTrue(c.getBoard().getTiles()[p.getRow()][p.getCol()].getRoom().equals("Kitchen"));
		c.getPlayers().get(0).setPos(5, 18);
		c.takingSecretPassage();
		p = c.getPlayers().get(0);
		assertTrue(c.getBoard().getTiles()[p.getRow()][p.getCol()].getRoom().equals("Conservatory"));
	}
}
