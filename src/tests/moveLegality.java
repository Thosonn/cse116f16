package tests;
/**
 * @author Darren Matthews
 */
import static org.junit.Assert.*;

import org.junit.Test;

import code.Clue;
import code.Player;

public class moveLegality {
	@Test
	public void throughWallIntoRoomTest(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,4);
		assertFalse("Moved into room without door",player1.move(5,3,0));
	}
	@Test
	public void outOfBoundsTest(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,4);
		assertFalse("Out of Bounds",player1.move(1,-1,0));
	}
	@Test
	public void intoStudyTest(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,4);
		assertTrue("Couldnt walk into study",player1.move(5,3,5));
	}
	@Test
	public void invalidSpaceTest(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,0);
		assertFalse("Moved to invalid space",player1.move(2,6,0));
	}
//	@Test
//	public void intoStudySecretPassage(){
//		Clue c = new Clue(2);
//		Player player1 = c.getPlayers().get(0);
//		Player player2 = c.getPlayers().get(1);
//		player2.setPos(3, 6);
//		player2.takingSecretPassage();
//		player1.setPos(3,6);
//		player1.move(1,3,0);
//		assertTrue("Couldnt get into Study Secret room",player1.getRow() == 23 && player1.getCol() == 18);
//		// testing takingSecretPassage also worked
//		assertTrue(player2.getRow() == 23 && player1.getCol() == 18);
//	}
	@Test
	public void horizontallySixSpaces(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,0);
		assertTrue("Moved more or less than 6 spaces",player1.move(6,5,6));
	}
	@Test
	public void verticallyFourSpaces(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,8);
		assertTrue("Moved more or less than 5 spaces",player1.move(5,9,8));
	}
	@Test
	public void upTwoRightTwo(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(6,6);
		assertTrue("Moved more or less than 4 spaces",player1.move(4,4,8));
	}
	@Test
	public void moveTooFar(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,0);
		assertFalse("Moved more than 3 spaces",player1.move(3,5,4));
	}
	@Test
	public void moveDiagonally(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(5,0);
		assertFalse("Moved diagonally in one move",player1.move(1,4,1));
	}
	@Test
	public void contiguousTest(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(23,9);
		assertFalse("Passed through room",player1.move(6,23,14));
	}
	@Test
	public void intoMiddleOfBoard(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(11,7);
		assertFalse("Entered middle of board",player1.move(6,13,10));
	}
	
	
	//rest of the tests are testing all the doors
	
	
	@Test
	public void intoLibrary1(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(6,6);
		assertTrue("Couldnt get into library first door",player1.move(4,8,6));
	}
	@Test
	public void intoLibrary2(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(10,6);
		assertTrue("Couldnt get into library second door",player1.move(6,10,3));
	}
	@Test
	public void intoConservatory(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(18,0);
		assertTrue("Couldnt get into conservatory",player1.move(5,19,4));
	}
	@Test
	public void intoBallroomLeft(){//far left door
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(16,8);
		assertTrue("Couldnt get into ballroom 1",player1.move(5,19,8));
	}
	@Test
	public void intoBallroomLeft2(){//left door
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(17,7);
		assertTrue("Couldnt get into ballroom 2",player1.move(4,17,9));
	}
	@Test
	public void intoBallroomRight(){//right door
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(16,15);
		assertTrue("Couldnt get into ballroom 3",player1.move(4,17,14));
	}
	@Test
	public void intoBallroomRight2(){//far right door
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(16,15);
		assertTrue("Couldnt get into ballroom 4",player1.move(5,19,15));
	}
	@Test
	public void intoKitchen(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(18,16);
		assertTrue("Couldnt get into kitchen",player1.move(5,18,19));
	}
	@Test
	public void intoDiningRoom(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(15,17);
		assertTrue("Couldnt get into dining room",player1.move(6,12,16));
	}
	@Test
	public void intoDiningRoom2(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(8,16);
		assertTrue("Couldnt get into dining room 2",player1.move(6,12,16));
	}
	@Test
	public void intoDiningRoom3(){
		Clue c = new Clue(1);
		Player player1 = c.getPlayers().get(0);
		player1.setPos(8,16);
		assertTrue("Couldnt get into dining room 3",player1.move(6,9,17));
	}
	
	
}
