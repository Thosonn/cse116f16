package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import code.Clue;
import gui.clueBoard;

public class DoorSelectingHandler implements ActionListener{
	private clueBoard _cb;
	private Clue _c;
	private int _row,_col;
	public DoorSelectingHandler(clueBoard cb, Clue c,int row,int col){
		_cb = cb;
		_c = c;
		_row = row;
		_col = col;
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_c.playerTurn().setPos(_row, _col);
		_cb.updateBoard();
	}

	}
