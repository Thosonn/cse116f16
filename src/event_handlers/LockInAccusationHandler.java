package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.clueBoard;

public class LockInAccusationHandler implements ActionListener{
	private clueBoard _cb;
	public LockInAccusationHandler(clueBoard cb){
		_cb = cb;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_cb.LockInAccUpdate();
	}
}
