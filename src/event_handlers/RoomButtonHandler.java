package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import code.roomCard;
import gui.clueBoard;

public class RoomButtonHandler implements ActionListener{
	private roomCard _c;
	private clueBoard _cb;
	public RoomButtonHandler(roomCard c, clueBoard cb){
		_cb =  cb;
		_c = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_cb.changeRoomSelected(_c);
	}
}
