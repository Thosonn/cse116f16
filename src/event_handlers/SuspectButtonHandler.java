package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import code.suspectCard;
import gui.clueBoard;

public class SuspectButtonHandler implements ActionListener {
	
	private suspectCard _c;
	private clueBoard _cb;
	public SuspectButtonHandler(suspectCard c, clueBoard cb){
		_cb =  cb;
		_c = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_cb.changeSuspectSelected(_c);
	}

}
