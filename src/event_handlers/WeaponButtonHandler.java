package event_handlers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import code.weaponCard;
import gui.clueBoard;

	public class WeaponButtonHandler implements ActionListener {
		private weaponCard _c;
		private clueBoard _cb;
		public WeaponButtonHandler(weaponCard c, clueBoard cb){
			_cb =  cb;
			_c = c;
		}
		



		@Override
		public void actionPerformed(ActionEvent e) {
			_cb.changeWeaponSelected(_c);
		}
}
