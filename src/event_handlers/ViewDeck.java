package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import code.Clue;
import gui.clueBoard;

public class ViewDeck implements ActionListener{
	private clueBoard _cb;
	
	
	public ViewDeck(Clue c, clueBoard cb){
		_cb = cb;

	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		_cb.showCardWindow();
	}
}
