package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import code.Clue;
import gui.clueBoard;

public class moveHandler implements ActionListener{
	private clueBoard _cb;
	private Clue _c;
	private int _rn;
	private int _cn;
	
	
	public moveHandler(Clue c, clueBoard cb, int rn, int cn){
		_cb = cb;
		_c = c;
		_rn = rn;
		_cn = cn;
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		_c.moveOneTileOnly(_c.playerTurn(), _c.playerTurn().getRow()+_rn, _c.playerTurn().getCol() + _cn);
		_cb.updateBoard();
	}
}
