package event_handlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.clueBoard;

public class SuggestionButtonHandler implements ActionListener{
	private clueBoard _cb;
	public SuggestionButtonHandler (clueBoard c){
		_cb = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_cb.openSugWindow();
	}
}
