package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import code.*;
import event_handlers.*;


public class clueBoard implements Observer{
	
	private Board _board;
	private Tiles[][] _boardArray;
	private JButton[][] _buttonArray; 
	private JPanel _panel;
	private JPanel _displayPanel;//panel that will displayer all the ultility buttons and cards
	private Clue _c ;
	private JPanel middlePanel;
	private JButton sugButton;
	private JLabel dicenum;
	private JLabel playerPlaying;
	private weaponCard weaponSelected =null;
	private suspectCard suspectSelected =null;
	private roomCard roomSelected =null;
	private JFrame frame;
	private JFrame window;
	private JFrame _hand = null;
	private JButton secPas;
	private JButton diceButton;
	private JButton existDoor;
	
	public clueBoard( Clue clue) {
		_c = clue;
		window=null;
		System.out.println("Answer Cards for the TA to check");
		for(int i=0;i<_c.getCulpritCards().size();i++){
			System.out.println(_c.getCulpritCards().get(i).getName());
		}
		_board = _c.getBoard();
		_c.getPlayers().get(0).setTurn(true);
		_c.addObserver(this);
		frame = new JFrame("Clue Game");
		_boardArray = _board.getTiles();
		_panel = new JPanel();
		_displayPanel = new JPanel();
		_panel.setPreferredSize(new Dimension(900,900));
		_panel.setBackground(new Color(75,150,120));
		_displayPanel.setBackground(Color.CYAN);
		_buttonArray = new JButton[Board.ROW][Board.COL];
		GridLayout layout = new GridLayout(Board.ROW, Board.COL);
		_panel.setLayout(layout);
		//display panel setup
		
		/**Displaying 4 move buutons
		 * 
		 */
		JPanel movePanel = new JPanel();
		movePanel.setLayout(new GridLayout(3,3));
		/** Player moving up
		 * 
		 */
		JButton up = new JButton("↑");
		up.addActionListener(new moveHandler(_c,this,-1,0));	
		up.setFont(new Font("Courier", Font.BOLD, 45));
		/**
		 * Player moving left
		 */
		JButton left = new JButton("←");
		left.addActionListener(new moveHandler(_c,this,0,-1));	
		left.setFont(new Font("Courier", Font.BOLD, 45));
		/**
		 * Player moving down
		 */
		JButton down = new JButton("↓");
		down.addActionListener(new moveHandler(_c,this,1,0));	
		down.setFont(new Font("Courier", Font.BOLD, 45));
		/**
		 * Player moving right 
		 */
		JButton right = new JButton("→");
		right.addActionListener(new moveHandler(_c, this,0,1));
		right.setFont(new Font("Courier", Font.BOLD, 45));
		
		
		
		/**
		 * middle panel will contain the dice, Player thats playing, suggestion button
		 */
		middlePanel = new JPanel();
		sugButton= new JButton("Making Suggestion");
		sugButton.setEnabled(false);
		sugButton.addActionListener(new SuggestionButtonHandler(this));
		
		/**
		 * generate a number from 1-6, will be disable after click, and enable for the next player
		 *  when the player end his/her turn 
		 */
		diceButton= new JButton("Roll Dice");
		dicenum = new JLabel("Roll The Die", SwingConstants.CENTER);
		ActionListener ac = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				_c.setDice(rollDice());
				secPas.setEnabled(false);
				dicenum.setText("Moves: " + _c.getDice());
				diceButton.setEnabled(false);
				
			}
	    };
	    diceButton.addActionListener(ac);
	    
	    
	    
	    /**
	     * ending the turn of the Player, allow next player to play
	     */
	    JButton endturn = new JButton("End Turn");
	    endturn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(_hand != null){
					_hand.setVisible(false);
				}
				sugButton.setEnabled(false);
				existDoor.setEnabled(false);
				_c.nextPlayerTurn();
				_c.setDice(0);
				_panel.removeAll();
				populateBoard();
				displayPlayer();
				_panel.repaint();
				if(_c.playerTurn().getRoom().equals("Study")||_c.playerTurn().getRoom().equals("Lounge")||
						_c.playerTurn().getRoom().equals("Kitchen")||_c.playerTurn().getRoom().equals("Conservatory")){
					secPas.setEnabled(true);
				}
				if(!_c.playerTurn().getRoom().equals("None")){
					existDoor.setEnabled(true);
				}
				diceButton.setEnabled(true);
				playerPlaying.setText(_c.playerTurn().getCharacter().getName() + "'s Turn");
				playerPlaying.setBackground(_c.playerTurn().getCharacter().getColor());
				dicenum.setText("Roll the die");
				middlePanel.repaint();
			}
	    	
	    });
	    
	    
	    movePanel.add(new JLabel());
		movePanel.add(up);
		movePanel.add(new JLabel());
		movePanel.add(left);
		movePanel.add(dicenum);
		movePanel.add(right);
		movePanel.add(diceButton);
		movePanel.add(down);
		movePanel.add(endturn);
	    
	    
	    
	    /**
	     * display the player that is playing
	     */
	    playerPlaying = new JLabel(_c.playerTurn().getCharacter().getName() + "'s Turn", SwingConstants.CENTER);
	    playerPlaying.setBackground(_c.playerTurn().getCharacter().getColor());
	    playerPlaying.setOpaque(true);
	    
	    middlePanel.setLayout(new GridLayout(5,1));
	    middlePanel.add(playerPlaying);
	    
		middlePanel.add(sugButton);
		secPas = new JButton ("Take Secret Passage");
		secPas.addActionListener(new SecPasHandler(this));
		secPas.setEnabled(false);
		middlePanel.add(secPas);
		existDoor = new JButton("Select Exit Door");
		existDoor.setEnabled(false);
		existDoor.addActionListener(new ExistDoorHandler(this));
		middlePanel.add(existDoor);
		

		/**
		 * bottom panel will displayer the player hand card
		 * and make accusastion
		 */
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(2,1));
		JButton viewDeck = new JButton("View Hand");
		viewDeck.addActionListener(new ViewDeck(_c,this));
		bottomPanel.add(viewDeck);
		JButton makeAcc = new JButton("Make Accusastion");
		makeAcc.addActionListener(new MakeAccusationButtonHandler(this));
		bottomPanel.add(makeAcc);
		
		_displayPanel.setLayout(new GridLayout(3,1));
		_displayPanel.setBackground(Color.BLACK);
		_displayPanel.add(movePanel);
		_displayPanel.add(middlePanel);
		_displayPanel.add(bottomPanel);
		
		this.populateBoard();
		frame.setLayout(new BorderLayout());
		frame.add(_panel,BorderLayout.WEST);
		frame.add(_displayPanel);
		frame.setVisible(true);
		frame.setSize(new Dimension(1250,1000));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	
	}
	public void setRoomText(){
		
		Map<TextAttribute, Object> attributes = new HashMap<TextAttribute, Object>();
		attributes.put(TextAttribute.TRACKING, 0.15);
		Font f2 = new Font("Courier", Font.BOLD, 12).deriveFont(attributes);
		_buttonArray[2][2].setText("Stu");
		_buttonArray[2][2].setFont(f2);
		_buttonArray[2][2].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[2][2].setVerticalAlignment(SwingConstants.TOP);
		_buttonArray[2][3].setText("dy");
		_buttonArray[2][3].setFont(f2);
		_buttonArray[2][3].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[2][3].setVerticalAlignment(SwingConstants.TOP);
		
		_buttonArray[8][2].setText("Libr");
		_buttonArray[8][2].setFont(f2);
		_buttonArray[8][2].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[8][3].setText("ary");
		_buttonArray[8][3].setFont(f2);
		_buttonArray[8][3].setHorizontalAlignment(SwingConstants.LEFT);
		
		_buttonArray[14][2].setText("Bill");
		_buttonArray[14][2].setFont(f2);
		_buttonArray[14][2].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[14][3].setText("iard");
		_buttonArray[14][3].setFont(f2);
		_buttonArray[14][3].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[15][2].setText("  Ro");
		_buttonArray[15][2].setFont(f2);
		_buttonArray[15][2].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[15][2].setVerticalAlignment(SwingConstants.TOP);
		_buttonArray[15][3].setText("om  ");
		_buttonArray[15][3].setFont(f2);
		_buttonArray[15][3].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[15][3].setVerticalAlignment(SwingConstants.TOP);
		
		_buttonArray[21][1].setText("Co");
		_buttonArray[21][1].setFont(f2);
		_buttonArray[21][1].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[21][1].setVerticalAlignment(SwingConstants.BOTTOM);
		_buttonArray[21][2].setText("nser");
		_buttonArray[21][2].setFont(f2);
		_buttonArray[21][2].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[21][2].setVerticalAlignment(SwingConstants.BOTTOM);
		_buttonArray[21][3].setText("vato");
		_buttonArray[21][3].setFont(f2);
		_buttonArray[21][3].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[21][3].setVerticalAlignment(SwingConstants.BOTTOM);
		_buttonArray[21][4].setText("ry");
		_buttonArray[21][4].setFont(f2);
		_buttonArray[21][4].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[21][4].setVerticalAlignment(SwingConstants.BOTTOM);
		
		_buttonArray[3][11].setText("Ha");
		_buttonArray[3][11].setFont(f2);
		_buttonArray[3][11].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[3][12].setText("ll");
		_buttonArray[3][12].setFont(f2);
		_buttonArray[3][12].setHorizontalAlignment(SwingConstants.LEFT);
		
		_buttonArray[20][11].setText("Ball");
		_buttonArray[20][11].setFont(f2);
		_buttonArray[20][11].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[20][12].setText("room");
		_buttonArray[20][12].setFont(f2);
		_buttonArray[20][12].setHorizontalAlignment(SwingConstants.LEFT);
		
		_buttonArray[3][20].setText("Lou");
		_buttonArray[3][20].setFont(f2);
		_buttonArray[3][20].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[3][20].setVerticalAlignment(SwingConstants.TOP);
		_buttonArray[3][21].setText("nge");
		_buttonArray[3][21].setFont(f2);
		_buttonArray[3][21].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[3][21].setVerticalAlignment(SwingConstants.TOP);
	
		_buttonArray[12][19].setText("Din");
		_buttonArray[12][19].setFont(f2);
		_buttonArray[12][19].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[12][20].setText("ing");
		_buttonArray[12][20].setFont(f2);
		_buttonArray[12][20].setHorizontalAlignment(SwingConstants.LEFT);
		_buttonArray[12][21].setText("Room");
		_buttonArray[12][21].setFont(f2);
		
		
		_buttonArray[21][20].setText("Kit");
		_buttonArray[21][20].setFont(f2);
		_buttonArray[21][20].setHorizontalAlignment(SwingConstants.RIGHT);
		_buttonArray[21][21].setText("chen");
		_buttonArray[21][21].setFont(f2);
		_buttonArray[21][21].setHorizontalAlignment(SwingConstants.LEFT);
		
		_buttonArray[3][0].setText("toKit");
		_buttonArray[3][0].setFont(new Font("Courier", Font.BOLD, 12));
		_buttonArray[19][1].setText("toLng");
		_buttonArray[19][1].setFont(new Font("Courier", Font.BOLD, 12));
		
		_buttonArray[5][Board.COL-1].setText("toCon");
		_buttonArray[5][Board.COL-1].setFont(new Font("Courier", Font.BOLD, 12));
		_buttonArray[Board.ROW-2][Board.COL-6].setFont(new Font("Courier", Font.BOLD, 12));
		_buttonArray[Board.ROW-2][Board.COL-6].setText("toStd");
	}
	
	/**
	 *First checks if there is another players card window open and closes it, then
	 *creates a new window with the new players cards and displays the graphics for them.
	 *Window size is based on how many cards the player has.
	 *End Turn button also closes the window.
	 */
	public void showCardWindow(){
		if(_hand != null){
			_hand.setVisible(false);
		}
		_hand = new JFrame("Player " + (_c.playerTurn().getPlayerNum()+1) + "'s Hand");
		_hand.setLocation(400,400);
		_hand.setLayout(new GridLayout(1, _c.playerTurn().getCards().size()));
		_hand.setSize(_c.playerTurn().getCards().size()*176, 280);
		_hand.setBackground(Color.BLACK);
		_hand.setVisible(true);
		
		for(int i = 0; i < _c.playerTurn().getCards().size(); i++){
			ImageIcon icon = new ImageIcon("cardImages/"+_c.playerTurn().getCards().get(i).getName()+".png");
			JLabel card = new JLabel(icon, SwingConstants.CENTER);
			card.setBackground(Color.BLACK);
			card.setBorder(BorderFactory.createBevelBorder(1,Color.BLACK, new Color(75,150,120)));
			_hand.add(card);
		}	
	}
	/**
	 * Exit door button update
	 * the door that player can select will be highlighted red and set enabled
	 * 
	 */
	public void existDoorUpdate(){
		existDoor.setEnabled(false);
		for (int x=0;x<Board.ROW;x++){
			for (int y=0;y<Board.COL;y++){
				if (_board.getTiles()[x][y].getRoom().equals(_c.playerTurn().getRoom())&&_board.getTiles()[x][y].getIsDoor()){
					_buttonArray[x][y].setBorder(new LineBorder(Color.RED));
					_buttonArray[x][y].setEnabled(true);
				}
			}
		}
	}
	/**
	 * display 6 players
	 *
	 */
	public void displayPlayer(){
		for (int i =0; i<_c.getPlayers().size();i++){
			Color c = _c.getPlayers().get(i).getCharacter().getColor();
			_buttonArray[_c.getPlayers().get(i).getRow()][_c.getPlayers().get(i).getCol()].setText("P " + (i+1));
			_buttonArray[_c.getPlayers().get(i).getRow()][_c.getPlayers().get(i).getCol()].setBackground(c);
			_buttonArray[_c.getPlayers().get(i).getRow()][_c.getPlayers().get(i).getCol()].setEnabled(true);
		}
		if(!_c.getLosePlayers().isEmpty()){
		for (int i =0; i<_c.getLosePlayers().size();i++){
			Color c = _c.getLosePlayers().get(i).getCharacter().getColor();
			_buttonArray[_c.getLosePlayers().get(i).getRow()][_c.getLosePlayers().get(i).getCol()].setText("P " + (i+1));
			_buttonArray[_c.getLosePlayers().get(i).getRow()][_c.getLosePlayers().get(i).getCol()].setBackground(c);
			_buttonArray[_c.getLosePlayers().get(i).getRow()][_c.getLosePlayers().get(i).getCol()].setEnabled(true);
		}
		}
		_panel.repaint();
		_panel.revalidate();
	}
	/**
	 * 
	 * generate the dice num
	 * @return random int from 1- 6
	 */
	public int rollDice(){
		Random r = new Random();
		return r.nextInt(6) +1;
	}
	
	/**
	 * Secret Passage Update
	 */
	public void secPasUpdate(){
		_c.takingSecretPassage();
		secPas.setEnabled(false);
		_panel.removeAll();
		populateBoard();
		displayPlayer();
		
	}

	/**
	 * display the board
	 */
	public void populateBoard(){
		
		for(int i = 0; i < Board.ROW; i++){
			for(int j = 0; j < Board.COL; j++){
				JButton button = new JButton();
				button.setEnabled(false);
				button.setOpaque(true);
				_buttonArray[i][j] = button;
				_panel.add(button);
				 if(_boardArray[i][j].isInvalid()){
					button.setBackground(new Color(75,150,120));
					button.setBorder(BorderFactory.createLineBorder(new Color(75,150,120)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getIsSecretDoor()){
					button.setBackground(Color.ORANGE);
					button.setBorder(BorderFactory.createLineBorder(Color.ORANGE));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getIsDoor()){
					button.setBackground(Color.PINK);
					button.setBorder(BorderFactory.createLineBorder(Color.PINK));
					button.addActionListener(new DoorSelectingHandler(this,_c,i,j));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Study"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Library"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Hall"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Conservatory"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Billiard Room"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Lounge"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Dining Room"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Kitchen"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else if(_boardArray[i][j].getRoom() == "Ballroom"){
					button.setBackground(new Color(245,245,245));
					button.setBorder(BorderFactory.createLineBorder(new Color(245,245,245)));
					_panel.add(button);
				}
				else{
					button.setBackground(new Color(220,220,35));
					button.setBorder(BorderFactory.createLineBorder(new Color(190,190,35)));
					_panel.add(button);
				}
			}
		}

		displayPlayer();
		setRoomText();
	}
	/**
	 * method call when make Accusation button clicked, similar to make suggestion window, but different in some way
	 */
	public void openAccWindow(){
		window = new JFrame("Make Accusation");
		window.setVisible(true);
		window.setSize(900, 600);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new GridLayout(1,4));
		JPanel weapon = new JPanel();
		weapon.setLayout(new GridLayout(4,2));
		weapon.add(new JLabel("     Select a"));
		weapon.add(new JLabel("Weapon: ↓"));
		//Cards weaponSelected =null;
		for (int i =0; i<_c.getWeaponOpt().size();i++){
			JButton button = new JButton(_c.getWeaponOpt().get(i).getName());
			button.setBackground(Color.RED);
			button.setOpaque(true);
			button.addActionListener(new WeaponButtonHandler(_c.getWeaponOpt().get(i),this));
			weapon.add(button);
		}
		JPanel suspect = new JPanel();
		suspect.setLayout(new GridLayout(4,2));
		suspect.add(new JLabel("     Select a"));
		suspect.add(new JLabel("Suspect: ↓"));
		//Cards weaponSelected =null;
		for (int i =0; i<_c.getSuspectOpt().size();i++){
			JButton button = new JButton(_c.getSuspectOpt().get(i).getName());
			button.setBackground(Color.GREEN);
			button.setOpaque(true);
			button.addActionListener(new SuspectButtonHandler(_c.getSuspectOpt().get(i),this));
			suspect.add(button);
		}
		JPanel room = new JPanel();
		room.setLayout(new GridLayout(5,2));
		room.add(new JLabel("select room:"));
		for (int i =0; i<_c.getRoomOpt().size();i++){
			JButton button = new JButton(_c.getRoomOpt().get(i).getName());
			button.setBackground(Color.BLUE);
			button.setOpaque(true);
			button.addActionListener(new RoomButtonHandler(_c.getRoomOpt().get(i),this));
			room.add(button);
		}
		JButton lockIn = new JButton("Lock in accusation");
		lockIn.setBackground(Color.LIGHT_GRAY);
		lockIn.addActionListener(new LockInAccusationHandler(this));
		window.add(weapon);
		window.add(suspect);
		window.add(room);
		window.add(lockIn);
		window.repaint();
	}
	/**
	 * update called after player locked in their accusation
	 * if not all three element selected the player will be ask to do it again
	 * other wise if the player won there will be message displaying he/she win
	 * else
	 * he will be remove from playing but still display on the board
	 */
	public void LockInAccUpdate(){
		if(weaponSelected==null||suspectSelected==null||roomSelected==null){
			System.out.println("incorrect Selection for the Accusation, Try again");
		}
		else {
			middlePanel.repaint();
			window.dispose();
			 if(_c.makeAccusation(roomSelected, suspectSelected, weaponSelected)){
				 window= new JFrame("Result");
				 window.setVisible(true);
				 //window.setSize(500, 300);
				 ImageIcon icon = new ImageIcon("ColorfulFireworksTry2.jpg");
				 JLabel win = new JLabel(icon, SwingConstants.CENTER);
			   	 win.setBackground(Color.BLACK);
				 window.add(win);
				 window.pack();
				 window.repaint();
			 }
			 else{
				 weaponSelected=null;
				 suspectSelected=null;
				 roomSelected=null;
				 window= new JFrame("Result");
				 window.setSize(500, 400);
				 window.setLayout(new GridLayout(4,1));
				 window.add(new JLabel("Wrong Accusation, You're out of the game"));
				 JLabel label1 = new JLabel();
				 label1.setBackground(Color.green);
				 label1.setOpaque(true);
				 JLabel label2 = new JLabel();
				 label2.setBackground(Color.pink);
				 label2.setOpaque(true);
				 JLabel label3 = new JLabel();
				 label3.setBackground(Color.cyan);
				 label3.setOpaque(true);
				 for(int i=0;i<_c.getCulpritCards().size();i++){
					 if(_c.getCulpritCards().get(i).getType().equals("Room")){
						label1.setText("Actual Room: "+_c.getCulpritCards().get(i).getName()); 
					 }
					 if(_c.getCulpritCards().get(i).getType().equals("Weapon")){
						label2.setText("Actual Weapon: "+_c.getCulpritCards().get(i).getName()); 
						}
					 if(_c.getCulpritCards().get(i).getType().equals("Suspect")){
						label3.setText("Actual Suspect: "+_c.getCulpritCards().get(i).getName()); 
					 }
				 }
				 window.add(label1);
				 window.add(label2);
				 window.add(label3);
				 window.setVisible(true);
				 /*
				  * calling ending turn 
				  * and remove the player from the playing list
				  */
					if(_hand != null){
						_hand.setVisible(false);
					}
					sugButton.setEnabled(false);
					existDoor.setEnabled(false);
					int playerRemove = _c.getPlayers().indexOf(_c.playerTurn());
					_c.nextPlayerTurn();
					_c.getLosePlayers().add(_c.getPlayers().remove(playerRemove));
					_c.setDice(0);
					_panel.removeAll();
					populateBoard();
					displayPlayer();
					_panel.repaint();
					if(_c.playerTurn().getRoom().equals("Study")||_c.playerTurn().getRoom().equals("Lounge")||
							_c.playerTurn().getRoom().equals("Kitchen")||_c.playerTurn().getRoom().equals("Conservatory")){
						secPas.setEnabled(true);
					}
					if(!_c.playerTurn().getRoom().equals("None")){
						existDoor.setEnabled(true);
					}
					diceButton.setEnabled(true);
					playerPlaying.setText(_c.playerTurn().getCharacter().getName() + "'s Turn");
					playerPlaying.setBackground(_c.playerTurn().getCharacter().getColor());
					dicenum.setText("Roll the die");
					middlePanel.repaint();
					if(_c.getPlayers().size()==1){
						System.out.println("Only one player left, automatically win!");
						frame.dispose();
					}
			 Timer time = new Timer();
				TimerTask task = new TimerTask(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
					window.dispose();	
					}
					
				};
				time.schedule(task, 7000);
		}
	}
	}
	/**
	 * Suggestion Button Handling
	 * Create a new Frame that will have option of selecting weapon and suspect, room will be 
	 * selected by default depends on which room the player is in
	 * once selected, click make suggestion button to submit your answer and see the result
	 */
	public void openSugWindow(){
		window = new JFrame("Make Sugeestion");
		window.setVisible(true);
		window.setSize(650, 600);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sugButton.setEnabled(false);
		frame.setEnabled(false);
		window.setLayout(new GridLayout(1,3));
		JPanel weapon = new JPanel();
		weapon.setLayout(new GridLayout(4,2));
		weapon.add(new JLabel("     Select a"));
		weapon.add(new JLabel("Weapon: ↓"));
		//Cards weaponSelected =null;
		for (int i =0; i<_c.getWeaponOpt().size();i++){
			JButton button = new JButton(_c.getWeaponOpt().get(i).getName());
			button.setBackground(Color.RED);
			button.setOpaque(true);
			button.addActionListener(new WeaponButtonHandler(_c.getWeaponOpt().get(i),this));
			weapon.add(button);
		}
		JPanel suspect = new JPanel();
		suspect.setLayout(new GridLayout(4,2));
		suspect.add(new JLabel("     Select a"));
		suspect.add(new JLabel("Suspect: ↓"));
		//Cards weaponSelected =null;
		for (int i =0; i<_c.getSuspectOpt().size();i++){
			JButton button = new JButton(_c.getSuspectOpt().get(i).getName());
			button.setBackground(Color.GREEN);
			button.setOpaque(true);
			button.addActionListener(new SuspectButtonHandler(_c.getSuspectOpt().get(i),this));
			suspect.add(button);
		}
		JPanel room = new JPanel();
		room.setLayout(new GridLayout(2,1));
		JPanel roomPanel1 = new JPanel();
		roomPanel1.setLayout(new GridLayout(1,2));
		roomPanel1.add(new JLabel("  Default Room: ↓"));
		roomPanel1.add(new JButton(_c.playerTurn().getRoom()));
		JPanel roomPanel2 = new JPanel();
		roomPanel1.setLayout(new GridLayout(2,1));
		roomPanel2.setLayout(new GridLayout(2,1));
		roomPanel2.add(new JLabel("Lock in selections:"));
		JButton makeSug = new JButton("Make Suggestion");
		makeSug.addActionListener(new MakeSuggestionHandler(this));
		roomPanel2.add(makeSug);
		room.add(roomPanel1);
		room.add(roomPanel2);
		window.add(weapon);
		window.add(suspect);
		window.add(room);
		window.repaint();
	}
	/**
	 * updating the GUI when player enter the room, the dice will set to 0 so the player
	 * can't make furtherm ove
	 * but the Suggestion button will be enable so the player are able to make suggestion
	 */
	@Override
	/**
	 * update method call when entering a room, or a secret passage that is taken
	 */
	public void update(Observable o, Object arg){
		_c.setDice(0);
		diceButton.setEnabled(false);
		sugButton.setEnabled(true);
	}
	/**
	 * update the board when the arrow key was clicked
	 */
	public void updateBoard(){
			_panel.removeAll();
			populateBoard();
			displayPlayer();
			dicenum.setText("Moves: "+_c.getDice());
			_panel.repaint();
			middlePanel.repaint();
	}
	/**
	 * Method call when player submit their selected suggestion answer
	 * @author Kun Lin
	 * check if the weapon or suspect selected is null, if true retry
	 * create a Frame that will display the answer
	 * 2 possible answer
	 * 1. No one can answer
	 * 2. there will be a display of Player and the Card proven suggestion wrong
	 * Note. do not make a new suggestion before the previous suggestion result window closed
	 * The window will close it self after several second
	 */
	public void updateSuggestion(){
		if(weaponSelected==null||suspectSelected==null){
			System.out.println("incorrect Selection for the Suggestion, Try again");
		}
		else {
			frame.setEnabled(true);
			sugButton.setEnabled(false);
			middlePanel.repaint();
			window.dispose();
			roomCard rc = new roomCard(_c.playerTurn().getRoom());
			 PlayerThatAnswerSuggestion answer = _c.MakingSuggestion(_c.playerTurn(), rc, suspectSelected, weaponSelected);
				weaponSelected=null;
				suspectSelected=null;
			 if(answer.getAnswerCard()==null|answer.getAnswerPlayer()==null){
				 window= new JFrame("Result");
				 window.setVisible(true);
				 window.setSize(500, 300);
				 window.add(new JLabel("No one able to prove suggestion wrong"));
				 window.repaint();
			 }
			 else{
				 window= new JFrame("Result");
				 window.setSize(500, 300);
				 window.setLayout(new GridLayout(3,1));
				 window.add(new JLabel("Suggestion proven wrong By following player and card"));
				 JLabel label1 = new JLabel();
				 label1.setLayout(new GridLayout(1,2));
				 label1.add(new JLabel("By Player:"));
				 JLabel pLabel = new JLabel(answer.getAnswerPlayer().getCharacter().getName());
				 pLabel.setBackground(answer.getAnswerPlayer().getCharacter().getColor());
				 pLabel.setOpaque(true);
				 label1.add(pLabel);
				 window.add(label1);
				 JLabel label2 = new JLabel();
				 label2.setLayout(new GridLayout(1,2));
				 label2.add(new JLabel("By"+ answer.getAnswerCard().getType()+"Card:"));
				 JLabel cLabel = new JLabel(answer.getAnswerCard().getName());
				 cLabel.setBackground(Color.PINK);
				 cLabel.setOpaque(true);
				 label2.add(cLabel);
				 window.add(label2);
				 window.setVisible(true);
				
			 }
			 Timer time = new Timer();
				TimerTask task = new TimerTask(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
					window.dispose();	
					}
					
				};
				time.schedule(task, 8000);
		}
	}
	public void windowDisplayAnswer(){
		window.removeAll();
	}
	public void changeSuspectSelected(suspectCard c){
		suspectSelected = c;
	}
	public suspectCard getSuspectSelected(){
		return suspectSelected;
	}
	public void changeWeaponSelected(weaponCard c){
		weaponSelected = c;
	}
	public weaponCard getWeaponSelected(){
		return weaponSelected;
	}
	public roomCard getRoomSelected(){
		return roomSelected;
	}
	public void changeRoomSelected(roomCard c){
		roomSelected = c;
	}
}
